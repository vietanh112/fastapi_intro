# fastAPI_intro

fast_API introduction

## Mục tiêu
làm quen với thư viện fast API
## Các chức năng cần thực hiện

- [ ] Dựng 1 website có các chức năng cơ bản: quản lý user, phân quyền user, ghi log ra file
- [ ] Chức năng chính: user upload file ảnh hóa đơn, hệ thống trả về các thông tin ocr trên ảnh đó
- [ ] Chức năng phụ: hiển thị 30 ảnh gần nhất mà tất cả user đã upload, có phân trang, mỗi trang hiển thị tối đa 10 ảnh
- [ ] Tất cả user đã login thành công đc thực hiện chức năng chính
- [ ] Chỉ user có vai trò admin mới được quyền xem chức năng phụ


## Timeline
26/8 có sản phẩm demo
