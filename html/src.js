let url = 'http://127.0.0.1:8000';


async function handClick(){   
    let username = document.getElementById('username').value
    let password = document.getElementById('password').value
    // https://zetcode.com/javascript/get-post-request/
    console.log(username)
    let data = {'username': username, 
                'password': password}
    // console.log(username, password)
    
    let res = await fetch(url + '/login', {
        method: 'POST', 
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    })

    if (res.ok) {
        let ret = await res.json();
        const {token, route} = ret;
        // console.log(token)
        localStorage.setItem("token", token)
        window.location.href = url + route  // Redirect
        // console.log(route)
    }
    else 
        console.log(`HTTP error: ${res.status}`)
}

async function loadImage(event){
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
}

async function uploadImg() {
    const formData  = new FormData();
    var image = document.getElementById('file').files[0];
    // console.log(image)
    formData.append("img", image)
    let res = await fetch(url + '/uploadimg/', {
        method: 'POST', 
        headers: {'Authorization': `Bearer ${localStorage.getItem("token")}`},
        // 'Content-Type': 'multipart/form-data',
        body: formData
    });

    if (res.ok) {
        let ret = await res.json();
        const {download, msg} = ret;
        console.log(msg)
        if (download != ''){
            window.open(url + download); // open newtab
        }
    }
    else 
        console.log(`HTTP error: ${res.status}`)
}

async function get_recent_photo(){
    let res = await fetch(url + '/get_recent_photo', {
        method:'POST',
        headers: {'Authorization': `Bearer ${localStorage.getItem("token")}`},
    })

    if (res.ok){
        let ret = await res.json();
        const {photos, num, msg} = ret;
        console.log(msg);
        console.log(typeof(photos));
        console.log(typeof(num), photos);

        let recent = document.getElementById('recent')
        photos.forEach(photo => {
          const imageEl = document.createElement("img");
          imageEl.setAttribute("width", 316)
          imageEl.setAttribute("src", photo)
          recent.appendChild(imageEl)

          const br = document.createElement("br");
          recent.appendChild(br)
          
        });
        // remove show recent buttom
    }
}
function remove_old_photos(recent){
    while (recent.firstChild) {
        recent.removeChild(recent.lastChild);
    }
}


async function get_recent_page(num){
    let res = await fetch(url + '/get_recent' + num, {
        method:'POST',
        headers: {'Authorization': `Bearer ${localStorage.getItem("token")}`},
    });

    if (res.ok){
        let ret = await res.json();
        const {photos, num, msg} = ret;
        let recent = document.getElementById('recent')  // tag div để hiển thị ảnh

        remove_old_photos(recent); //remove old photo

        photos.forEach(photo => {
            const imageEl = document.createElement("img");
            imageEl.setAttribute("width", 316);
            imageEl.setAttribute("src", photo);
            const br = document.createElement("br");

            recent.appendChild(imageEl);
            recent.appendChild(br);
          });
    }
}
