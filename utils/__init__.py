from glob import glob
from time import time
import re
from collections import deque
import posixpath as p
from typing import Optional

def set_image_name() -> str:
    return re.sub('.', '-', str(time()).strip('.0')) + '.jpg'

recent_path = 'upload/recent.txt'

def save_recent(queue:deque) -> None:
    with open(recent_path, 'w', encoding='utf-8') as f:
        for elem in queue:
            f.write(elem + '\n')

def load_recent(path:Optional[str]=None) -> deque:
    queue = deque(maxlen=30)
    if (not p.isfile(path)) or (path is None):
        imgs = sorted(glob(p.join('upload', '*.jpg')), key=p.getmtime)
        for imgp in imgs:
            queue.append(r'/' + imgp)
        return queue
    
    with open(path, 'r', encoding='utf-8') as f:
        lines = f.read().strip().splitlines()

    for line in lines:
        queue.append(line)
    
    return queue