from collections import deque
import posixpath as p
from time import time
from typing import Any, Union

import jwt
from fastapi import Depends, FastAPI, HTTPException, Request, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from PIL import Image
from PIL.ImageFile import ImageFile
from pydantic import BaseModel
from retrying import retry
from rich import print
from starlette.responses import FileResponse

from security import validate_token
from utils import set_image_name, save_recent, load_recent

templates = Jinja2Templates(directory='html')

SECURITY_ALGORITHM = 'HS256'
SECRET_KEY = '123456'
ACCOUNTS = [
    ("admin", "admin"),
    ("admin1", "admin1"),
    ("user0", "123456"),
    ("user1", "123456"),
    ("user2", "123456"),
    ("user3", "123456"),
]

ROLE = [True, True, False, False, False, False] # True admin, False user, None account not exist

recent_photo = load_recent('upload/recent.txt')
if len(recent_photo) == 0:
    recent_photo.append('/upload/2.jpg')
    recent_photo.append('/upload/4.jpg')
    recent_photo.append('/upload/4.jpg')

def generate_jwt(username: Union[str, Any]) -> str:
    
    expire = time() + 60 * 60 # xpired after 1 hour
    to_encode = {"exp": expire, 
                 "username": username}
    # print(type(to_encode), to_encode)
    return jwt.encode(to_encode, SECRET_KEY, algorithm=SECURITY_ALGORITHM)


class LoginRequest(BaseModel):
    username: str
    password: str
    

def verify_user(username:str, password:str) -> bool:
    for role, acc in zip(ROLE, ACCOUNTS):
        if acc == (username, password):
            return role
            # break
    return None


app = FastAPI()
app.mount("/html"    , StaticFiles(directory='html'    ), name="static"  )
app.mount("/download", StaticFiles(directory='download'), name="download")
app.mount("/upload"  , StaticFiles(directory='upload'  ), name="upload"  )
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/login")
async def loginPage(request: Request):
    return templates.TemplateResponse('login2.html', {'request': request})

@app.post('/login')
def login(login_data: LoginRequest):
    # print(f'[x] request_data: {login_data.__dict__}')
    role = verify_user(username=login_data.username, 
                       password=login_data.password)
    if role is not None:
        if role: route = '/admin'
        else: route = '/user'
        
        jwt_token = generate_jwt(login_data.username)
        return {'token': jwt_token,
                'route': route}
    else:
        raise HTTPException(status_code=404, detail="User not found")   # redirect to page login


# @retry(wait_random_min=32, wait_random_max=3000, stop_max_attempt_number=3)
# @app.post("/uploadimg/", dependencies=[Depends(reusable_oauth2)])
@app.post("/uploadimg/", dependencies=[Depends(validate_token)])
async def create_upload_img(img: UploadFile):
    try: 
        pic = Image.open(img.file).convert('RGB')
    except: 
        return{'download': '',
               'msg':'image is invalid'}
    
    if isinstance(pic, ImageFile):
        path = p.join('upload', set_image_name())
        pic.save(path)
        recent_photo.append(r'/{}'.format(path.strip(r'/')))
        save_recent(recent_photo)
    # path = 'data/maplestory_bluesnail.png'
    # return FileResponse(path=path, filename=p.basename(path))
    return {'download': r'/download/HĐBV-VB2-CH-BVL.xlsx',
            'msg':'success'}


# @app.get("/user", dependencies=[Depends(validate_token)])
@app.get("/user")
async def userPage(request: Request):
    return templates.TemplateResponse('user.html', {'request': request})

# @app.get("/admin", dependencies=[Depends(validate_token)])
@app.get("/admin")
async def adminPage(request: Request):
    return templates.TemplateResponse('admin.html', {'request': request})


@app.post("/get_recent_photo", dependencies=[Depends(validate_token)])
async def get_recent_photo():
    return {'photos': recent_photo,
            'num': recent_photo.__len__(),
            'msg': 'success'}
    
@app.post("/get_recent1", dependencies=[Depends(validate_token)])
async def get_recent1():
    imgs = list(recent_photo)[:10]
    return {'photos': recent_photo,
            'num': len(imgs),
            'msg': 'success'}
    
@app.post("/get_recent2", dependencies=[Depends(validate_token)])
async def get_recent2():
    imgs = list(recent_photo)[10:20]
    return {'photos': imgs,
            'num': len(imgs),
            'msg': 'success'}
    
@app.post("/get_recent3", dependencies=[Depends(validate_token)])
async def get_recent3():
    imgs = list(recent_photo)[20:30]
    return {'photos': imgs,
            'num': len(imgs),
            'msg': 'success'}


# if __name__ == '__main__':
#     import uvicorn
#     uvicorn.run(
#         'main:app',
#         host='localhost', 
#         port=1611, 
#         log_level='info', 
#         workers=2,
#         reload=True,
#     )
